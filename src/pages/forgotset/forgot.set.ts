import { Component } from '@angular/core';
import {App} from '../../app/app.component';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'forgot-set-page',
  templateUrl: 'forgot.set.html'
})
export class ForgotSetPage {
  private input: any = {
    email: null,
    recoveryCode: null,
    currentPassword: null,
    currentPasswordVerify: null
  };
  constructor(
    public nav: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
    ) {
      this.input.email = this.navParams.get("email");
  }
  
  setPassword() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(App.apiUrl + 'changePassword', JSON.stringify(this.input), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['successMessage'],
          buttons: ['OK']
        });
        alert.present();
        this.nav.popToRoot();
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return App.uiLabelMap;
  }
}
